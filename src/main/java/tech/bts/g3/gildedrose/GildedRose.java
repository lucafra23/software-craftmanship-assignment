package tech.bts.g3.gildedrose;

class GildedRose {

    Item[] items;

    public GildedRose(Item[] items) {
        this.items = items;
    }

    public update(Item[] items) {
        for (Item item : items) {
            switch (item.name) {
                case "SULFURAS":
                    updateSulfuras(item);
                    break;
                case "Aged Brie":
                    updateAgedBrie(item);
                    break;
                case "Backstage":
                    updateBackstage(item);
                default:
                    godefault(items);
                    break;

            }
        }
    }

    private void godefault(Item[] items) {

    }

    private void updateAgedBrie(Item item) {

            if (!item.name.equals("Aged Brie") && (item.quality > 0)) {
                if (item.quality < 50) {
                    item.quality += 1;

                    if (item.sellIn < 6) {
                        item.quality += 1;
                    }
                } else {

                    item.quality -= item.quality;
                }
            }
    }


    public void updateSulfuras(Item item) {

            if (!item.name.equals("Sulfuras, Hand of Ragnaros") && (item.quality > 0)) {
                item.quality -= 1;
            }
            else {
                    if (item.quality < 50) {
                        item.quality += 1;
                    }
            }
    }



    public void updateBackstage (Item item) {

            if (item.name.equals("Backstage passes to a TAFKAL80ETC concert")) {

                if (item.sellIn < 11) {

                    if (item.quality < 50) {
                        item.quality += 1;
                    }

                    if (item.sellIn < 6) {
                        if (item.quality < 50) {
                            item.quality += 1;

                        }

                    }

                } else {
                    item.quality -= item.quality;

                }

            }

        }

}
